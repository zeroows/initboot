package tk.aalkhodiry.grocerylist.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(locations = "classpath:properties/db.properties", ignoreUnknownFields = false, prefix = "db")
public class DatabaseProperties {
    private Connection connection;
    private Hibernate hibernate;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Hibernate getHibernate() {
        return hibernate;
    }

    public void setHibernate(Hibernate hibernate) {
        this.hibernate = hibernate;
    }

    public static class Connection {
        private String driver;
        private String url;
        private String username;
        private String password;

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Hibernate {
        private String packagesToScan;
        private String hbm2ddl;
        private boolean generateDdl;
        private boolean showSql;
        private boolean formatSql;

        public String getPackagesToScan() {
            return packagesToScan;
        }

        public void setPackagesToScan(String packagesToScan) {
            this.packagesToScan = packagesToScan;
        }

        public String getHbm2ddl() {
            return hbm2ddl;
        }

        public void setHbm2ddl(String hbm2ddl) {
            this.hbm2ddl = hbm2ddl;
        }

        public boolean isGenerateDdl() {
            return generateDdl;
        }

        public void setGenerateDdl(boolean generateDdl) {
            this.generateDdl = generateDdl;
        }

        public boolean isShowSql() {
            return showSql;
        }

        public void setShowSql(boolean showSql) {
            this.showSql = showSql;
        }

        public boolean isFormatSql() {
            return formatSql;
        }

        public void setFormatSql(boolean formatSql) {
            this.formatSql = formatSql;
        }
    }

}
