package tk.aalkhodiry.grocerylist.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the Category database table.
 *
 */
@Entity
@Table(name = "GL_CATEGORIES")
public class Category {
    @Id
    @Column
    private int id;

    @Column
    private String name;

    @OneToMany(mappedBy = "category", targetEntity = Item.class, fetch = FetchType.LAZY)
    private List<Item> items;

    @Column(nullable = false, insertable = false, updatable = false)
    private long timestamp;

    @Version
    @Column(nullable = false, insertable = false, updatable = false)
    private long version;

    @PrePersist
    public void prePersist() {
        timestamp = new Date().getTime();
    }
}
