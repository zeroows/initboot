package tk.aalkhodiry.grocerylist.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the Category database table.
 *
 */
@Entity
@Table(name = "GL_ITEMS")
public class Item {
    @Id
    @Column
    private int id;

    @Column
    private String name;

    @ManyToOne
    private Category category;

    @Column(nullable = false, insertable = false, updatable = false)
    private long timestamp;

    @Version
    @Column(nullable = false, insertable = false, updatable = false)
    private long version;

    @PrePersist
    public void prePersist() {
        timestamp = new Date().getTime();
    }
}
