package tk.aalkhodiry.grocerylist.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ControllerAdvice
// To Handle Exceptions
public class ExceptionController {
    private static final Log log = LogFactory.getLog(ExceptionController.class);

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ExceptionHandler({ RuntimeException.class, Exception.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, String> runTimeError(Exception e) {
        Throwable baseCause = e;
        Map<String, String> exception = new HashMap<String, String>();

        /**
         * Extracting the base cause of the exception
         */
        while (baseCause.getCause() != null) {
            baseCause = baseCause.getCause();
        }

        String uniqId = UUID.randomUUID().toString();
        String error = String.format("An Error Occured - UUID: %s, \ncause: %s", uniqId, baseCause.getMessage());

        log.error(error, e);

        exception.put("code", "500");
        exception.put("reason", String.format("Runtime Error Occured, Please view Logs for more details."));

        return exception;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(value=HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map<String, String> NoHandlerFound() {
        Map<String, String> exception = new HashMap<String, String>();

        exception.put("code", "404");
        exception.put("reason", String.format("Bad url, Handler Not Found."));


        log.error(exception.get("reason"));

        return exception;
    }

//    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    @ExceptionHandler(AccessDeniedException.class)
//    @ResponseStatus(HttpStatus.FORBIDDEN)
//    @ResponseBody
//    Map<String, String> accessDenied() {
//        Map<String, String> exception = new HashMap<String, String>();
//
//        exception.put("code", "403");
//        exception.put("reason", "Access Denied");
//
//        return exception;
//    }
}
