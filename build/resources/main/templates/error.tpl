yieldUnescaped '<!DOCTYPE html>'
html(lang:'en') {
    head {
        meta('http-equiv':'"Content-Type" content="text/html; charset=utf-8"')
        title('No Found - 404')
    }
    body {
        p('Not Found - Page Doesn\'t exists')
    }
}